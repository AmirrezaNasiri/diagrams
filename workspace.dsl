workspace {
  model {
    guest = person "Guest"
    user = person "Verified User"
    
    emailProvider = softwareSystem "Email Provider" {
      tags "External System"
    }
    
    system = softwareSystem "Online Work Managment Platform" {
      database = container "Database" "" {
        technology "Mysql"
        tags "Database"
      }

      fileStorage = container "File Storage" "Store uploded files" {
        technology "S3"
      }

      apiSystem = container "API System" {
        emailService = component "Email Service" {
          -> emailProvider "Send emails"
        }

        notificationManager = component "Notification Manager" {
          -> emailProvider "Send notification emails"
          // TODO: Broadcast push notifications
        }

        authService = component "Authentication Service" {
          -> database "Store/retrieve user information"
          -> emailService "Send verification email"
        }

        accountService = component "Account Service" {
          -> database "Update user information"
          -> emailService "Send verification email"
        }

        accessService = component "Access Manager (IAM)" {
          -> database "Uses to authorize users"
        }

        outputGeneratorService = component "Project Exporter Service" {
          -> fileStorage "Upload generated files"
        }

        projectAnalyzerService = component "Projcet Analyzer Service" {
          -> database "Prepare desired report"
          -> outputGeneratorService "Generate outputs"
          -> accessService "Authorize access"
          -> notificationManager "Notify user"
        }

        projectService = component "Project Service" {
          -> database "Update and store project information"
          -> accessService "Authorize access"
        }



      }
      
    }

    guest -> authService "Signup"
    guest -> authService "Signin"
    guest -> authService "Email verification"
    guest -> authService "Request password reset"

    guest -> emailProvider "Read verification email"

    user -> accountService "Update account information"
    user -> projectService "Store or update project"
    user -> projectAnalyzerService "Request a report analysis"

  }
  views {
    systemlandscape "SystemLandscape" {
      include *
      autoLayout
    }
    
    systemContext system "system-highest-abstraction"{
      include *
      autolayout
    }
    
    systemContext emailProvider "email-highest-abstraction"{
      include *
      autolayout
    }
    
    container system "system-containers" {
      include *
      autolayout
    }

    component apiSystem "api-system-components" {
      include *
      autolayout
    }

    dynamic apiSystem "update-project" "Update project information." {
      user -> projectService "Request a project to be updated"
      projectService -> database "Retrieve project information for future use"
      projectService -> accessService "Can user update this resource?"
      accessService -> projectService "Yes"
      projectService -> database "Update information"
      autoLayout
    }

    dynamic apiSystem "generate-report-files" "Generate a project-specific report" {
      // user -> projectAnalyzerService "Request a report" {
      //   technology "Dispach as a queue"
      // }
      user -> projectAnalyzerService "Request a report"
      projectAnalyzerService -> database "Retrieve basic project info"
      projectAnalyzerService -> accessService "Can user generate this report?"
      accessService -> projectAnalyzerService "Yes"
      projectAnalyzerService -> database "Prepare report data (Dispatch as a queue)"
      projectAnalyzerService -> outputGeneratorService "Order to generate output files (Dispatch as a queue)"
      outputGeneratorService -> fileStorage "Upload generated files"
      projectAnalyzerService -> notificationManager "Notify uploaded files"
      autoLayout
    }
   
    theme default
    
    styles {
       element "External System" {
        background #999999
        color #ffffff
      }
      
      element "Database" {
        shape Cylinder
      }
    }
  }
}